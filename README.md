# Web animáció - Animációs film beadandó 

## Feladat leírása
Készíts egy animációt egy pattogó labdáról!

A labda folyamatosan távolodjon a szemlélőtől.

Figyelj az életszerűségre! Tartsd szem előtt a korábban, elméleti részekben szereplő útmutatásokat!

A megvalósításhoz magad választhatod ki az eszközt: scratch; html5, css3 vagy valamelyik korábban felsorolt alkalmazás.

## Futtatási információk
Javasolt böngésző: Google Chrome

De ugyanúgy működik ezekben a böngészőkben is: Opera, Mozzila Firefox

Az index.html fájlt megnyitva látható a pattogó labda animáció.

## Fejlesztési információk
HTML5 canvas-t valamint javascriptet használtam a fejlesztés során
