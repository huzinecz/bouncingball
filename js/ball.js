const Ball = {

    FPS: 60, // Képkocka/másodperc
    gravity: 0.3, // esés gyorsasága, 0 és 1 közötti szám, minél kissebb annál lassabb
    bounceFactor: 0.8, // labda pattogási ereje, tehát minél nagyobb annál nagyobbat pattan a labda (0 és 1 közötti szám)
    radius: 70, // labda mérete
    distancing: 0.2, // távolodás mértéke. Minél nagyobb annál gyorsabban távolodik

    canvas : null,
    ctx: null,
    ball: null,

    init: function(){
        this.canvas = document.getElementById("myCanvas");
        this.setSizes();
        this.ctx = this.canvas.getContext("2d");

        this.addBall();
    },

    /**
     * Méretek kiszámítása
     */
    setSizes: function(){
        this.canvas.height = window.innerHeight;
        this.canvas.width = window.innerWidth;

        let minsize = this.canvas.width;
        if(this.canvas.height < this.canvas.width){
            minsize = this.canvas.height;
        }

        this.radius = parseInt(minsize/4);
        this.distancing = this.radius/600;
    },

    /**
     * Labda definiálása
     */
    addBall: function(){
        this.ball = new this.Ball(this.canvas.width/2, 0, 0, 1, this.radius);  
    
        // Kép betöltése
        this.img=new Image(this.radius, this.radius);
        this.img.onload = function() {
            // Miután betöltődött, elindíjuk a pattogást
            Ball.bounceBallStart();
        };
        this.img.src="img/ball3.png";
    },


    /**
     * Labda objektum
     */
    Ball: function(x, y, vx, vy, radius) {
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        this.origX = x;
        this.origY = y;
        this.radius = radius;
    },

    /**
     * Pattogás indítása
     */
    bounceBallStart: function(){
        const that = this;
        const ball = this.ball;
        const H = this.canvas.height;


        let interval = setInterval(function(){ 
            ball.y += ball.vy;
            ball.vy += that.gravity;
            if(ball.y + ball.radius > H) { // leért a "földre"
                ball.y = H - ball.radius;
                ball.vy *= -that.bounceFactor;
                if(ball.vy > -1){
                    clearInterval(interval);
                }
            }
            // folyamatosan kicsinyíjük
            ball.radius -= that.distancing;

            that.updateCanvas();
            if(ball.radius < 2){
                clearInterval(interval);
                setTimeout(function(){
                    that.init();
                }, 3000);
            }
           // clearInterval(interval);
        }, 1000/this.FPS);
    },

    /**
     * Rajzvászon frissítése
     */
    updateCanvas: function(){
        var pat=this.ctx.createPattern(this.img,"repeat");
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.beginPath();
        this.ctx.drawImage(this.img,this.ball.x-this.ball.radius/2, this.ball.y, this.ball.radius, this.ball.radius);
        this.ctx.fillStyle = pat;
        this.ctx.fill();
    },

}


Ball.init();
